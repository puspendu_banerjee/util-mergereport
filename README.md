
#Merge Report Utility

## Description
This utility generates a report on Reltio merges in a csv or delimited format.  The utility is configured for a specified entity type and a list of attributes to display in the report.
Details please refer to the development spec in source.

##Change Log

```
#!plaintext
Update Date: 10/01/2019
Version: 1.3.1
Description: Added client credential , Reltio Core CST v1.4.9 updated.

Update Date: 27/06/2019
Version: 1.3
Description: Proxy Change and standardizing the Jar name and implement filter capabilities


Update Date: 27/03/2019
Version: 1.2.6
Description: Added password encryption logic, added proper validation message incase required property is missing

Update Date: 13/11/2018
Version: 1.2.5
Description: Added performance log

Update Date: 13/11/2018
Version: 1.2.4
Description: Fixed output bug resulting in incorrect csv output

Update Date: 11/03/2017
Version: 1.2.0
Description: Fixed output bug resulting in incorrect csv output

Update Date: 09/14/2017
Version: 1.1.0
Description: Included support for multi-value attributes. Implemented comma-delimited attribute/source lists in main configuration to support integration with web UI.

Update Date: 08/30/2017
Version: 1.0.0
Description: Initial version
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-mergereport/src/ca902cdc355ebbf433cfddd16599f364af3b539c/QuickStart.md?at=master&fileviewer=file-view-default).


