package com.reltio.ps.MergeReport;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;

import junit.framework.Assert;

public class MergeReportTest {

    private static final Logger LOGGER = LogManager.getLogger(MergeReportTest.class.getName());

    private static Gson gson = new Gson();
    private static DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    private static List<String> attributesForReport;
    private static BufferedWriter br;
    private static SynchronizedBufferedWriter mr;
    private static long frmDte;
    private static long toDte;
    private static String mergeType;
    private static String threadCount;
    private static TokenGeneratorService tokenGeneratorService = null;
    private static ConsistencyCounter consistencyCounter = new ConsistencyCounter();
    private static ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(
            tokenGeneratorService);
    /*
     * Fetching values from property file
     */
    private static String AUTH_URL;
    private static String API_URL;
    private static String mergeReport;
    private static String username;
    private static String password;
    private static String entityType;
    private static String fromDate;
    private static String toDate;
    private static String attributesPath;
    private static String batchSize;

    static {

        Properties properties = new Properties();
        try {
            InputStream in = new MergeReportTest().getClass()
                    .getResourceAsStream("/UtilityProperty.properties");
            properties.load(in);
        } catch (Exception e) {
            LOGGER.error("Failed reading properties File...");
            e.printStackTrace();
        }
        /*
         * READ the Properties values
         */
        AUTH_URL = properties.getProperty("AUTH_URL");
        API_URL = properties.getProperty("API_URL");
        mergeReport = properties.getProperty("MERGE_OUTPUT_FILE_LOCATION");
        username = properties.getProperty("USERNAME");
        password = properties.getProperty("PASSWORD");
        entityType = properties.getProperty("ENTITY_TYPE");
        fromDate = properties.getProperty("FROM_DATE");
        toDate = properties.getProperty("TO_DATE");
        attributesPath = properties.getProperty("ATTRIBUTES_MAPPING");
        batchSize = properties.getProperty("BATCH_SIZE");
        mergeType = properties.getProperty("MERGE_TYPE");
        threadCount = properties.getProperty("THREAD_SIZE");

        if (batchSize == null) {
            batchSize = "100";
        }
    }

    /*
     * Utility Method
     *
     */
    private static long convertDateToLong(String date) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(date));
        long lDate = cal.getTimeInMillis();
        return lDate;
    }

    private static long waitForTasksReady(Collection<Future<Long>> futures,
                                          int maxNumberInList) throws Exception {
        long totalResult = 0l;
        while (futures.size() > maxNumberInList) {
            Thread.sleep(20);
            for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
                if (future.isDone()) {
                    futures.remove(future);
                }
            }
        }
        return totalResult;
    }

    @Test
    public void creatingFileWithHeader() {

        Mapping mapping = new Mapping(attributesPath);
        try {
            mapping.loadAttributes();
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        attributesForReport = mapping.attributes;
        Date date = new Date();

        try {
            File file = new File(mergeReport + "_" + sdf.format(date) + ".txt");
            br = new BufferedWriter(new FileWriter(file));
            mr = new SynchronizedBufferedWriter(br);

            frmDte = convertDateToLong(fromDate);
            toDte = convertDateToLong(toDate);

            String header = "golden_Uri|win_Uri|lose_Uri|MergeRule|MergeReason|MergeUser|MergeTime|CreateTime";
            for (String attribute : attributesForReport) {
                header += "|win_" + attribute + "|lose_" + attribute;
            }
            mr.write(header);
            mr.newLine();
            mr.flush();

            Assert.assertTrue(file.exists());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }

    @Test
    public void countOfResult() {

        try {
            tokenGeneratorService = new TokenGeneratorServiceImpl(username,
                    password, AUTH_URL);

            final ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(
                    tokenGeneratorService);
            final String totalURL = API_URL
                    + "_total?filter=equals(type,'configuration/entityTypes/"
                    + entityType + "') and range(updatedTime,"
                    + convertDateToLong(fromDate) + ","
                    + convertDateToLong(toDate) + ")";

            String totalResponse = reltioAPIService.get(totalURL);
            Integer total = JsonPath.read(totalResponse, "$.total");
            consistencyCounter.setTotal(Integer.valueOf(total));

            Assert.assertTrue(101 == total);
        } catch (APICallFailureException e) {
            LOGGER.error("Error Code: " + e.getErrorCode()
                    + " >>>> Error Message: " + e.getErrorResponse());
        } catch (GenericException e) {
            LOGGER.error(e.getExceptionMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }

    @Test
    public void fetchDataBasedOnEntityType() {
        String intitialJSON = "";
        int threadsNumber = 1;
        ExecutorService executorService = Executors
                .newFixedThreadPool(threadsNumber);

        try {
            final String incReportURL = API_URL
                    + "_scan?filter=equals(type,'configuration/entityTypes/"
                    + entityType + "') and range(updatedTime,"
                    + convertDateToLong(fromDate) + ","
                    + convertDateToLong(toDate) + ")&select=uri&max="
                    + batchSize;

            String scanResponse = reltioAPIService.post(incReportURL,
                    intitialJSON);
            if (scanResponse != null && !"".equals(scanResponse)) {
                LOGGER.debug("Got response fetchDataSpecificToEntityType");
            } else {
                LOGGER.debug(
                        "Does not got response fetchDataSpecificToEntityType");
            }
            executorService.shutdown();
            Boolean success = consistencyCounter.isComplete();
            Assert.assertTrue(success);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

}
