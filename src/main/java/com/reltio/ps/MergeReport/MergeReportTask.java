/**
 * 
 */
package com.reltio.ps.MergeReport;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.reltio.cst.service.ReltioAPIService;

/**
 * @author sanjay
 *
 */
public class MergeReportTask implements Callable<Long>{

	private static final Logger LOGGER = LogManager.getLogger(MergeReportTask.class.getName());

	private static String QUOTE = "\"";
	private static Gson gson = new Gson();
	private static final String ATTR = "attributes";

	
	private List<HashMap> urisMap;
	private int[] count;
	private ReltioAPIService reltioAPIService;
	private String tenant_url;
	private ConsistencyCounter consistencyCounter;

	private static String mergeType;
	private static long frmDte;
	private static long toDte;
	private static String delimiter;
	private static List<String> attributesForReport;
	private static SynchronizedBufferedWriter mr;
	
	public MergeReportTask(List<HashMap> urisMap, int[] count, ReltioAPIService reltioAPIService, String tenant_url, ConsistencyCounter consistencyCounter, 
			SynchronizedBufferedWriter mr, String mergeType, long frmDte, long toDte, String delimiter, List<String> attributesForReport) {
		this.urisMap = urisMap;  
		this.count = count;
		this.reltioAPIService = reltioAPIService;
		this.tenant_url = tenant_url;
		this.consistencyCounter = consistencyCounter;
		MergeReportTask.mr = mr;
		MergeReportTask.mergeType = mergeType;
		MergeReportTask.frmDte = frmDte;
		MergeReportTask.toDte = toDte;
		MergeReportTask.delimiter = delimiter;
		MergeReportTask.attributesForReport = attributesForReport;
	}
	
	@Override
	public Long call() throws Exception {
        long requestExecutionTime = 0L;
        long startTime = System.currentTimeMillis();
		for (HashMap map : urisMap) {
			count[0] += 1;
			String attributesJson = gson.toJson(map);
			String entityID=map.get("uri").toString().substring(9);
			String resp = reltioAPIService
					.get(tenant_url + entityID + "/_crosswalkTree");
			consistencyCounter.incrementCounter();
			try {
				processCrosswalkTree(entityID, resp, attributesJson);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		LOGGER.info("Scanned records count >>> " + count[0]);
		mr.flush();
        requestExecutionTime = System.currentTimeMillis()
                - startTime; 
        return requestExecutionTime;
        
	}
	
	/**
	 * Process Crosswalktree and analyze the winer and loser
	 * @param rootUri
	 * @param jsonMerge
	 * @param jsonEntity
	 */
	private static void processCrosswalkTree(String rootUri, String jsonMerge, String jsonEntity) {

		try {
			String source = JsonPath.read(jsonMerge, "$.crosswalks[0].uri");
			String type = JsonPath.read(jsonMerge, "$.crosswalks[0].type");
			if (type.endsWith("ReltioCleanser")) {
				source = JsonPath.read(jsonMerge, "$.crosswalks[1].uri");
				type = JsonPath.read(jsonMerge, "$.crosswalks[1].type");
			}
			String uri = JsonPath.read(jsonMerge, "$.uri");
			String createTime = "";
			Long createdTime = ((Double)(JsonPath.read(jsonEntity, "$.createdTime"))).longValue();
			try {
				createTime = convertMillsToDate(createdTime.toString(), "EST");
			} catch (Exception e) {
				LOGGER.debug("Invalid value in createTime attribute");
			}

			Map<String, String> winAttributes = new HashMap<String, String>();
			Map<String, String> winAttributeUris = new HashMap<String, String>();

			Map<String, String> loseAttributes = new HashMap<String, String>();
			Map<String, String> loseAttributesUris = new HashMap<String, String>();

			populateAttributeUris(jsonEntity, source, type, winAttributeUris);
			populateAttributeMap(jsonEntity, winAttributes, winAttributeUris);

			populateAttributeUris(jsonEntity, source, type, loseAttributesUris);
			populateAttributeMap(jsonEntity, loseAttributes, loseAttributesUris);

			Object oMerges = null;
			List<Object> merges = new ArrayList<Object>();
			int i = 0;
			try {
				oMerges = JsonPath.read(jsonMerge, "$.merges");
				Boolean nextExists = true;
				while (nextExists) {
					Object merge = JsonPath.read(oMerges, "$[" + i + "]");
					if (merge != null) {
						merges.add(merge);
					} else {
						nextExists = false;
					}
					i++;
				}
			} catch (Exception e) {
				if (i == 0 && oMerges != null) {
					Object merge = oMerges;
					merges.add(merge);
				}
			}
			if (i > 1) {
			}

			if (oMerges != null) {
			}

			for (Object merge : merges) {

				Map<String, Object> temp = (Map<String, Object>) merge;
				String time = temp.get("time").toString();
				long mergeTime = Long.parseLong(time);
				if ((mergeTime >= frmDte) && (mergeTime <= toDte)) {
					if ("" == mergeType || "false".equals(mergeType)) {
						processMerge(rootUri, createTime, uri, source, merge, jsonEntity, loseAttributes);
					} else {
						processMerge(rootUri, createTime, uri, source, merge, jsonEntity, winAttributes);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Skipped empty record...\n"+e.getLocalizedMessage());
		}
	}
	
	/**
	 * Convert milliseconds to Date
	 * @param date
	 * @param timeZone
	 * @return
	 * @throws Exception
	 */
	private static String convertMillsToDate(String date, String timeZone) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.parseLong(date));
		Date d = cal.getTime();
		TimeZone tz = TimeZone.getTimeZone(timeZone);
		dateFormat.setTimeZone(tz);
		return dateFormat.format(d);
	}

	
	/**
	 * Process the Merge and create the output csv
	 * @param rootUri
	 * @param createTime
	 * @param winUri
	 * @param winSource
	 * @param jsonMerge
	 * @param jsonEntity
	 * @param winAttributes
	 */
	private static void processMerge(String rootUri, String createTime, String winUri, String winSource,
			Object jsonMerge, String jsonEntity, Map<String, String> winAttributes) {

		Map<String, String> mergeDetails = new HashMap<String, String>();
		populateMergeDetails(jsonMerge, mergeDetails);

		List<Object> losers = new ArrayList<Object>();
		int i = 0;
		try {
			Boolean nextExists = true;
			while (nextExists) {
				Object loser = JsonPath.read(jsonMerge, "$.losers[" + i + "]");
				if (loser != null) {
					losers.add(loser);
				} else {
					nextExists = false;
				}
				i++;
			}
		} catch (Exception e) {
			if (i < 1) {
				e.printStackTrace();
			}
		}
		if (i > 1) {
		}
		for (Object loser : losers) {
			processLoser(rootUri, createTime, winUri, winSource, loser, jsonEntity, winAttributes, mergeDetails);
		}

	}

	private static void processLoser(String rootUri, String createTime, String winUri, String winSource,
			Object jsonLoser, String jsonEntity, Map<String, String> winAttributes, Map<String, String> mergeDetails) {

		String uri = JsonPath.read(jsonLoser, "$.uri");
		String source = "";
		String type = "";

		try {
			Boolean entityFlag = false;
			List<Object> crosswalks = JsonPath.read(jsonLoser, "$.crosswalks[*]");
			for (int i = 0; i < crosswalks.size(); i++) {
				try {
					String s = JsonPath.read(crosswalks.get(i), "$.ownerType");
					if (s.equals("entity")) {
						source = JsonPath.read(crosswalks.get(i), "$.uri");
						type = JsonPath.read(crosswalks.get(i), "$.type");

						Map<String, String> loseAttributes = new HashMap<String, String>();
						Map<String, String> loseAttributeUris = new HashMap<String, String>();

						populateAttributeUris(jsonEntity, source, type, loseAttributeUris);
						populateAttributeMap(jsonEntity, loseAttributes, loseAttributeUris);

						writeLine(winAttributes, loseAttributes, mergeDetails, winUri, uri, rootUri, createTime);
						entityFlag = true;

					}
				} catch (PathNotFoundException pnfe) {
					LOGGER.error("rootUri=" + rootUri + ", uri=" + uri + " -- Entity crosswalk not found.  Skipped");
				} catch (Exception e) {
					LOGGER.error("jsonLoser: " + jsonLoser);
					LOGGER.error("rootUri: " + rootUri);
					e.printStackTrace();
				}
			}

			if (!entityFlag) {
				LOGGER.error("rootUri=" + rootUri + ", uri=" + uri + " -- Entity crosswalk not found.  Skipped");
			}
		} catch (PathNotFoundException pnfe) {
			LOGGER.error("rootUri=" + rootUri + ", uri=" + uri + " -- Nested Merges Found!!!!");

			List<Object> childMerges = new ArrayList<Object>();
			int j = 0;
			try {
				Boolean nextExists = true;
				while (nextExists) {
					Object childMerge = JsonPath.read(jsonLoser, "$.merges[" + j + "]");
					if (childMerge != null) {
						childMerges.add(childMerge);
					} else {
						nextExists = false;
					}
					j++;
				}
			} catch (Exception e) {
				if (j >= 1) {
					LOGGER.debug("Nested merges found...");
				}
			}

			for (Object childMerge : childMerges) {
				Map<String, Object> temp = (Map<String, Object>) childMerge;
				String time = temp.get("time").toString();
				long mergeTime = Long.parseLong(time);
				if ((mergeTime >= frmDte) && (mergeTime <= toDte)) {
					processMerge(rootUri, createTime, uri, source, childMerge, jsonEntity, winAttributes);
				}
			}

		} catch (Exception e) {
			LOGGER.error("jsonLoser: " + jsonLoser);
			LOGGER.error("rootUri: " + rootUri);
			e.printStackTrace();
		}

	}

	private static void populateAttributeUris(String jsonEntity, String source, String type,
			Map<String, String> attrUris) {
		String attrJsonPath = "$.crosswalks[?(@.uri=='" + source + "')].attributes[*]";
		List<String> attributeUris = JsonPath.read(jsonEntity, attrJsonPath);
		String refJsonPath = "$.attributes.*[*].refEntity.crosswalks[?(@.type=='" + type + "')].attributeURIs[*]";

		List<String> refAttributeUris  = new ArrayList<>();
		
		try {
			refAttributeUris = JsonPath.read(jsonEntity, refJsonPath);
		}catch (Exception e) {
			LOGGER.info("Attributes not present");
		}

		for (String attributeUri : attributeUris) {
			String[] attrUriSegments = attributeUri.split("/");
			int len = attrUriSegments.length;
			String temp = "";
			Boolean isFirst = true;
			for (int i = 0; i < (len - 3) / 2; i++) {
				if (isFirst) {
					isFirst = false;
					temp += attrUriSegments[3 + 2 * i];
				} else {
					temp += ".";
					temp += attrUriSegments[3 + 2 * i];
				}
			}
			attrUris.put(temp, attributeUri);
		}

		for (String refAttributeUri : refAttributeUris) {
			String[] attrUriSegments = refAttributeUri.split("/");
			int len = attrUriSegments.length;
			String temp = "";
			Boolean isFirst = true;
			for (int i = 0; i < (len - 3) / 2; i++) {
				if (isFirst) {
					isFirst = false;
					temp += attrUriSegments[3 + 2 * i];
				} else {
					temp += ".";
					temp += attrUriSegments[3 + 2 * i];
				}
			}
			attrUris.put(temp, refAttributeUri);
		}

	}

	private static void populateAttributeMap(String jsonEntity, Map<String, String> attributeMap,
			Map<String, String> attributeUris) {

		HashMap entity = JsonPath.read(jsonEntity, "$");
		entity.containsKey(ATTR);
		
		
		if(entity.containsKey(ATTR)) {
			for (String attribute : attributeUris.keySet()) {
				String[] attributeSegments = attribute.split("\\.");
				Boolean isFirst = true;
				String jsonPath = "$";

				for (int i = 0; i < attributeSegments.length; i++) {
					if (isFirst) {
						isFirst = false;
						jsonPath += ".attributes." + attributeSegments[i];
					} else {
						jsonPath += "[*].value." + attributeSegments[i];
					}
				}

				jsonPath += "[?(@.uri=='" + attributeUris.get(attribute) + "')].value";
				List<String> results = JsonPath.read(jsonEntity, jsonPath);
				String result = "";
				try {
					result = results.get(0);
				} catch (Exception e) {
				}
				attributeMap.put(attribute, result);

			}
		}
		
	}

	private static void writeLine(Map<String, String> winnerAttributes, Map<String, String> loserAttributes,
			Map<String, String> mergeDetails, String winUri, String loseUri, String rootUri, String createTime) {

		String line = "";
		try {
			line += QUOTE + rootUri + QUOTE + delimiter + QUOTE + winUri + QUOTE + delimiter + QUOTE + loseUri + QUOTE
					+ delimiter + QUOTE + mergeDetails.get("mergeRules") + QUOTE + delimiter + QUOTE
					+ mergeDetails.get("mergeReason") + QUOTE + delimiter + QUOTE + mergeDetails.get("user") + QUOTE
					+ delimiter + QUOTE + convertMillsToDate(mergeDetails.get("time"), "EST") + QUOTE + delimiter
					+ QUOTE + createTime + QUOTE;
		} catch (Exception e1) {
			line += QUOTE + rootUri + QUOTE + delimiter + QUOTE + winUri + QUOTE + delimiter + QUOTE + loseUri + QUOTE
					+ delimiter + QUOTE + mergeDetails.get("mergeRules") + QUOTE + delimiter + QUOTE
					+ mergeDetails.get("mergeReason") + QUOTE + delimiter + QUOTE + mergeDetails.get("user") + QUOTE
					+ delimiter + QUOTE + "failed to parse: " + QUOTE + mergeDetails.get("time") + QUOTE + delimiter
					+ QUOTE + createTime + QUOTE;
		}

		for (String attribute : attributesForReport) {
			String temp = winnerAttributes.get(attribute);
			if (temp == null) {
				temp = "";
			}
			line += delimiter + QUOTE + temp + QUOTE;

			temp = loserAttributes.get(attribute);

			if (temp == null) {
				temp = "";
			}

			line += delimiter + QUOTE + temp + QUOTE;
		}
		try {
			mr.write(line + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	
	private static void populateMergeDetails(Object jsonMerge, Map<String, String> mergeAttributes) {
		Map<String, Object> temp = (Map<String, Object>) jsonMerge;

		String time = temp.get("time").toString();
		String user = temp.get("user").toString();
		String mergeReason = temp.get("mergeReason").toString();
		String mergeRules = temp.get("mergeRules").toString();

		mergeAttributes.put("time", time);
		mergeAttributes.put("user", user);
		mergeAttributes.put("mergeReason", mergeReason);
		mergeAttributes.put("mergeRules", mergeRules);

	}

	
}
