package com.reltio.ps.MergeReport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author Mohan
 */
public class AttributeObject {

	public String label;
	
	public Map<String, List<Object>> value = new HashMap<String, List<Object>>();

}
