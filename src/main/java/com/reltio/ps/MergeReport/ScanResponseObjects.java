package com.reltio.ps.MergeReport;

import com.reltio.cst.domain.Attribute;

import java.util.HashMap;
import java.util.List;

public class ScanResponseObjects {
	private Attribute cursor;

	private List<HashMap> objects;

	public Attribute getCursor() {
		return cursor;
	}

	public void setCursor(Attribute cursor) {
		this.cursor = cursor;
	}

	public List<HashMap> getObjects() {
		return objects;
	}

	public void setObjects(List<HashMap> objects) {
		this.objects = objects;
	}

}
