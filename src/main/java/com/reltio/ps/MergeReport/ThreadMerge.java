package com.reltio.ps.MergeReport;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import com.google.gson.JsonSyntaxException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.util.Util;

import javax.net.ssl.HttpsURLConnection;

/* Steps to generate report --
 *
 * Objective:
 *   Surviving values and losing record values (but... what about the winner which never loses?)
 *
 * 1. Get entity w/ uri X
 * 2. Get surviving attributes
 * 3. Under "crosswalks", identify associated sources
 *    - determine source and cwId
 * 4. For each merge (not including HCS)
 *    - Get attribute Uris based on source / cwId
 *    - get attributes based on Attribute Uri
 * 5. Generate output record based on gathered information
 *
 * ** An alternative strategy, which may be more correct..
 * 1. Get every merge by winner and loser.
 * 2. Apply above for both winner and loser Uris
 * NOTE: This approach may require including the "winner", "loser", and the "golden record" all in the report.  Could become cumbersome
*/

/**
 *
 * @author Sanketha CR
 */
public class ThreadMerge {

    private static final Logger LOGGER = LogManager.getLogger(ThreadMerge.class.getName());
	private static final Logger JSON_PARSE_ERROR_LOGGER = LogManager.getLogger("com.bcbsnc.dna.ErrorLogger");
	private static final Logger logPerformance = LogManager.getLogger("performance-log");

	private static Gson gson = new Gson();

	private static DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	private static List<String> attributesForReport;
	private static BufferedWriter br;
	private static SynchronizedBufferedWriter mr;

	/*
	 * Fetching values from property file
	 */
	private static String auth_url;
	private static String tenant_id;
	private static String environment_url;
	// private static String API_URL;
	private static String mergeReport;
	private static String username;
	private static String password;
	private static String entityType;
	private static String fromDate;
	private static String toDate;
	private static String attributesPath;
	private static String pageSize;
	private static long frmDte;
	private static long toDte;
	private static String mergeType;
	private static String threadCount;
	private static String delimiter;
	private static String filter;

	private static String QUOTE = "\"";

	public static void main(String[] args) throws Exception {
		long programStartTime = System.currentTimeMillis();

		Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");
		/*
		 * READ the Properties values
		 */
		auth_url = properties.getProperty("AUTH_URL");
		tenant_id = properties.getProperty("TENANT_ID");
		environment_url = properties.getProperty("ENVIRONMENT_URL");
		mergeReport = properties.getProperty("MERGE_OUTPUT_FILE_LOCATION");
		username = properties.getProperty("USERNAME");
		password = properties.getProperty("PASSWORD");
		entityType = properties.getProperty("ENTITY_TYPE");
		fromDate = properties.getProperty("FROM_DATE");
		toDate = properties.getProperty("TO_DATE");
		attributesPath = properties.getProperty("ATTRIBUTES_MAPPING");
		pageSize = properties.getProperty("RECORDS_PER_POST");
		mergeType = properties.getProperty("MERGE_TYPE");
		threadCount = properties.getProperty("THREAD_COUNT");
		delimiter = properties.getProperty("DELIMITER", "|");
		filter = properties.getProperty("FILTER");

		Util.setHttpProxy(properties);
		final String tenant_url = "https://" + environment_url + "/reltio/api/" + tenant_id + "/" + "entities" + "/";

		LOGGER.debug("tenant_url: " + tenant_url);

		if (pageSize == null) {
			pageSize = "100";
		}        
        Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));
		
        List<String> missingKeys = Util.listMissingProperties(properties,Arrays.asList("AUTH_URL","MERGE_OUTPUT_FILE_LOCATION", "ENVIRONMENT_URL", "ENTITY_TYPE", "TENANT_ID","ATTRIBUTES_MAPPING","FROM_DATE","TO_DATE"), mutualExclusiveProps);

       if (!missingKeys.isEmpty()) {
            System.out.println(
                    "Following properties are missing from configuration file!! \n" + String.join("\n", missingKeys));
            System.exit(0);
        }
		Mapping mapping = new Mapping(attributesPath);
		mapping.loadAttributes();
		attributesForReport = mapping.attributes;

		Date date = new Date();

		br = new BufferedWriter(new FileWriter(mergeReport + "_" + sdf.format(date) + ".csv"));
		mr = new SynchronizedBufferedWriter(br);

		frmDte = convertDateToLong(fromDate);
		toDte = convertDateToLong(toDate);

		final ConsistencyCounter consistencyCounter = new ConsistencyCounter();

		String header = QUOTE + "golden_Uri" + QUOTE + delimiter + QUOTE + "win_Uri" + QUOTE + delimiter + QUOTE
				+ "lose_Uri" + QUOTE + delimiter + QUOTE + "MergeRule" + QUOTE + delimiter + QUOTE + "MergeReason"
				+ QUOTE + delimiter + QUOTE + "MergeUser" + QUOTE + delimiter + QUOTE + "MergeTime" + QUOTE + delimiter
				+ QUOTE + "CreateTime" + QUOTE;

		for (String attribute : attributesForReport) {
			header += delimiter + QUOTE + "win_" + attribute + QUOTE + delimiter + QUOTE + "lose_" + attribute + QUOTE;
		}

		mr.write(header);
		mr.newLine();
		mr.flush();

		final ReltioAPIService reltioAPIService = Util.getReltioService(properties);

		String intitialJSON = "";

		final String totalURL = tenant_url + "_total?"+consructFilterUrl(entityType, fromDate, toDate, null, filter);

		final String incReportURL = tenant_url + "_scan?"+consructFilterUrl(entityType, fromDate, toDate, pageSize, filter)+"&select="+getSelectField(attributesForReport);

		String totalResponse = reltioAPIService.get(totalURL);

		LOGGER.debug("totalResponse: " + totalResponse);

		Integer total = JsonPath.read(totalResponse, "$.total");
		consistencyCounter.setTotal(Integer.valueOf(total));

		LOGGER.info(consistencyCounter.getTotal());

		LOGGER.debug("URL==" + incReportURL);

		boolean eof = false;
        long totalFuturesExecutionTime = 0L;
        final int MAX_QUEUE_SIZE_MULTIPLICATOR = 10;

		final int[] count = { 0 };
		int threadsNumber = Integer.parseInt(threadCount);
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadsNumber);
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();

		while (!eof) {
			for (int i = 0; i < threadsNumber * MAX_QUEUE_SIZE_MULTIPLICATOR; i++) {

				String scanResponse = reltioAPIService.post(incReportURL, intitialJSON);
				if (scanResponse != null && !"".equals(scanResponse)) {
					ScanResponseObjects scanResponseObjs = null;
					try {
						scanResponseObjs = gson.fromJson(scanResponse, ScanResponseObjects.class);
					}catch (JsonSyntaxException jse){
						JSON_PARSE_ERROR_LOGGER.error("Could not parse: \n"+scanResponse);
						throw jse;
					}
					if (scanResponseObjs.getObjects() != null && scanResponseObjs.getObjects().size() > 0) {
						final List<HashMap> uris = scanResponseObjs.getObjects();
						/*
						 * Starting the individual threads for each entity uri
						 */
						MergeReportTask mergeTask = new MergeReportTask(uris, count, reltioAPIService, tenant_url, consistencyCounter, mr, 
								mergeType, frmDte, toDte, delimiter, attributesForReport);
						Future<Long> f = executorService.submit(mergeTask);
						futures.add(f);

					} else {
						eof = true;
						break;
					}
					scanResponseObjs.setObjects(null);
					intitialJSON = gson.toJson(scanResponseObjs.getCursor());
					intitialJSON = "{\"cursor\":" + intitialJSON + "}";
				}
			}
			
            totalFuturesExecutionTime += waitForTasksReady(futures, threadsNumber*(MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
            printPerformanceLog(executorService.getCompletedTaskCount() * Integer.parseInt(pageSize), totalFuturesExecutionTime, programStartTime, threadsNumber);
            
		}
        totalFuturesExecutionTime += waitForTasksReady(futures, 0);
        printPerformanceLog(executorService.getCompletedTaskCount() * Integer.parseInt(pageSize), totalFuturesExecutionTime, programStartTime, threadsNumber);
        
		executorService.shutdown();
		Boolean success = consistencyCounter.isComplete();
		if (success) {
			LOGGER.debug("Process completed successfully...");
		} else {
			LOGGER.debug("Processes completed unsuccessfully...");
		}

		mr.close();
		
		System.out.println("Extract process Completed.....");
		long finalTime = System.currentTimeMillis() - programStartTime;
		System.out.println("[Performance]:  Total processing time : "
				+ (finalTime / 1000) + "  Seconds");

		if (success) {
			System.exit(0);
		} else {
			System.exit(-1);
		}
	}
	
	private static String getSelectField(List<String> attributesForReport) {
		StringBuffer selectFlds = new StringBuffer();
		String attrStr = "attributes.";
		String comma = ",";
		
		selectFlds.append("createdTime"+comma);
		selectFlds.append("crosswalks"+comma);
		selectFlds.append("uri"+comma);
		for (String attr : attributesForReport) {
			selectFlds.append(attrStr+attr+comma);
		}
		selectFlds.replace(selectFlds.length()-1, selectFlds.length(), "");

		return selectFlds.toString();
	}
	
	private static String consructFilterUrl(String entityType, String fromDate, String toDate, String pageSize, String filter) throws Exception {
		StringBuilder urlBuilder = new StringBuilder();

		urlBuilder.append("filter=equals(type,'configuration/entityTypes/");
		urlBuilder.append(entityType);
		urlBuilder.append("') and range(updatedTime,");
		urlBuilder.append(convertDateToLong(fromDate));
		urlBuilder.append(",");
		urlBuilder.append(convertDateToLong(toDate));
		urlBuilder.append(")");

		if( filter != null) {
			urlBuilder.append(" and ");
			urlBuilder.append(filter);
		}

		if( pageSize != null) {
			urlBuilder.append("&max=");
			urlBuilder.append(pageSize);
		}

		return urlBuilder.toString();
	}

    /**
     * Waits for futures (load tasks list put to executor) are partially ready.
     * <code>maxNumberInList</code> parameters specifies how much tasks could be
     * uncompleted.
     *
     * @param futures         - futures to wait for.
     * @param maxNumberInList - maximum number of futures could be left in "undone" state.
     * @return sum of executed futures execution time.
     */
    public static long waitForTasksReady(Collection<Future<Long>> futures,
                                         int maxNumberInList) {
        long totalResult = 0l;
        while (futures.size() > maxNumberInList) {
            try {
                Thread.sleep(20);
            } catch (Exception e) {
                // ignore it...
            }
            for (Future<Long> future : new ArrayList<Future<Long>>(futures)) {
                if (future.isDone()) {
                    try {
                        totalResult += future.get();
                        futures.remove(future);
                    } catch (Exception e) {
                    	LOGGER.error(e.getMessage());
                    	LOGGER.debug(e);
                    }
                }
            }
        }
        return totalResult;
    }



	private static long convertDateToLong(String date) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat.parse(date));
		long lDate = cal.getTimeInMillis();
		return lDate;
	}



	
	public static void printPerformanceLog(long totalTasksExecuted,
			long totalTasksExecutionTime,
			long programStartTime, long numberOfThreads) {
		LOGGER.info("[Performance]: ============= Current performance status ("
				+ new Date().toString() + ") =============");
		long finalTime = System.currentTimeMillis() - programStartTime;
		LOGGER.info("[Performance]:  Total processing time : "
				+ finalTime);

		LOGGER.info("[Performance]:  Entities Processed: "
				+ totalTasksExecuted);
		LOGGER.info("[Performance]:  Total OPS (Entities Processed / Time spent from program start): "
				+ (totalTasksExecuted / (finalTime / 1000f)));
		LOGGER.info("[Performance]:  Total OPS without waiting for queue (Entities Processed / Time spent from program start): "
				+ (totalTasksExecuted / ((finalTime) / 1000f)));
		LOGGER.info("[Performance]:  API Server data load requests OPS (Entities Processed / (Sum of time spend by API requests / Threads count)): "
				+ (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
		LOGGER.info("[Performance]: ===============================================================================================================");

		//log performance only in separate logs
		logPerformance.info("[Performance]: ============= Current performance status ("
				+ new Date().toString() + ") =============");
		logPerformance.info("[Performance]:  Total processing time : "
				+ finalTime);

		logPerformance.info("[Performance]:  Entities Processed: "
				+ totalTasksExecuted);
		logPerformance.info("[Performance]:  Total OPS (Entities Processed / Time spent from program start): "
				+ (totalTasksExecuted / (finalTime / 1000f)));
		logPerformance.info("[Performance]:  Total OPS without waiting for queue (Entities Processed / Time spent from program start): "
				+ (totalTasksExecuted / ((finalTime) / 1000f)));
		logPerformance.info("[Performance]:  API Server data load requests OPS (Entities Processed / (Sum of time spend by API requests / Threads count)): "
				+ (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
		logPerformance.info("[Performance]: ===============================================================================================================");
	}

	/**
	 * @return the filter
	 */
	public static String getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public static void setFilter(String filter) {
		ThreadMerge.filter = filter;
	}

}
