package com.reltio.ps.MergeReport;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.reltio.cst.socket.LocalAddressSocketFactory;
import com.reltio.cst.socket.LocalBindAwareSSLSocketFactory;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.io.IOUtils;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpUriRequest;
//import org.apache.http.client.methods.RequestBuilder;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

public class ApiConnect {
    private static Logger LOGGER= LoggerFactory.getLogger(ApiConnect.class);
    public static void main(String ... args) throws IOException, NoSuchAlgorithmException {
        final LocalBindAwareSSLSocketFactory sslSocketFactory=new LocalBindAwareSSLSocketFactory();
        HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
        ConnectionSpec requireTls12 = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .build();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient client =new OkHttpClient().newBuilder()
                .socketFactory(new LocalAddressSocketFactory(SocketFactory.getDefault()))
                .connectionSpecs(Arrays.asList(requireTls12))
                //.addInterceptor(logging)
                //.addNetworkInterceptor(logging)
                .followRedirects(true)
                .followSslRedirects(true)
                .build();
        ApiConnect apiConnect=new ApiConnect();
        String authBearer=apiConnect.getAuthBearer("puspendu.banerjee@bcbsnc.com","Welcome11",client);
        String entityData="";
        try {
            entityData=apiConnect.getEntity(client,authBearer);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getEntity(OkHttpClient client,String authBearer) throws IOException {
        //client = client==null? new OkHttpClient().newBuilder().build():client;
        Request request = new Request.Builder()
                .url("https://prod-h360.reltio.com/reltio/api/9I1pnaNzcZAXYFJ/entities/y7FCERI")
                .method("GET", null)
                .addHeader("Authorization", "Bearer "+authBearer)
                .build();
        Response response = client.newCall(request).execute();
        String respStr=response.body().string();
        LOGGER.info(respStr);
        return respStr;
    }
//    private String getEntity(HttpClient client, String authBearer) throws IOException{
//        String urlOverHttps
//                = "https://prod-h360.reltio.com/reltio/api/9I1pnaNzcZAXYFJ/entities/y7FCERI";
//        InetAddress localAddress=InetAddress.getByName("10.205.232.79");
//        RequestConfig a=RequestConfig.custom()
//                .setLocalAddress(localAddress).build();
//        client = client != null ? client : HttpClients.custom()
//                .setDefaultRequestConfig(a).build();
//        HttpUriRequest request = RequestBuilder
//                .get()
//                .setUri(urlOverHttps)
//                .setHeader("Authorization","Bearer "+authBearer)
//                .setHeader("Accept-Encoding","gzip, deflate, br")
//                .build();
//        HttpResponse response = client.execute(request);
//        String resp=IOUtils.toString(response.getEntity().getContent(),"UTF-8");
//        LOGGER.info(resp);
//        return resp;
//    }

    private String getEntity(String authBearer) throws Exception {
        String urlOverHttps
                = "https://prod-h360.reltio.com/reltio/api/9I1pnaNzcZAXYFJ/entities/y7FCERI";
        URL url = new URL(urlOverHttps);
        LOGGER.info("Final Url " + url.toString());
        //Proxy proxy=new Proxy(Proxy.Type.HTTP, new InetSocketAddress(localAddress,11080));
        URLConnection conn=url.openConnection();
        HttpsURLConnection connection = (HttpsURLConnection)conn;
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization","Bearer "+authBearer);
        //connection.setRequestProperty("Accept-Encoding","gzip, deflate");
        InputStream inputStream = connection.getInputStream();
        String contentEncoding=connection.getContentEncoding();
        contentEncoding=contentEncoding==null?"":contentEncoding;
        String resp="";
        switch (contentEncoding.toLowerCase()){
            case "gzip":
                resp=IOUtils.toString(new GZIPInputStream(inputStream),"UTF-8");
                break;
            case "deflate":
                resp=IOUtils.toString(new ZipInputStream(inputStream),"UTF-8");
                break;
            default:
                resp=IOUtils.toString(inputStream,"UTF-8");
        }

        LOGGER.info(resp);
        return resp;
    }

    private String getAuthBearer(String username, String password,OkHttpClient client) throws IOException {
        client = client==null? new OkHttpClient().newBuilder().build():client;
        String urlStr="https://auth02.reltio.com/oauth/token?username="+username+"&password="+password+"&grant_type=password";
        Request request = new Request.Builder()
                .url(urlStr)
                .method("GET", null)
                .header("Authorization","Basic cmVsdGlvX3VpOm1ha2l0YQ==")
                .build();
        Response response = client.newCall(request).execute();
        String respStr=response.body().string();
        LOGGER.info(respStr);
        JsonObject jsonObject = new JsonParser().parse(respStr).getAsJsonObject();
        //SimpleEntity entity = gson.fromJson(responseBody.string(), SimpleEntity.class);
        return jsonObject.get("access_token").getAsString();
    }

    private static X509TrustManager getAllTrustingTrustManager(){
        // Create a trust manager that does not validate certificate chains
        X509TrustManager trustAllCerts = new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers(){
                return new java.security.cert.X509Certificate[]{};
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        };
        return trustAllCerts;
    }

}
