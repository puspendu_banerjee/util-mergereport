package com.reltio.ps.MergeReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// Construct with mapping file path
// Using filereader, load attributes from mapping file into an attribute list

public class Mapping {
	
	String filePath;
	List<String> attributes;
	List<String> sources;
	
	public Mapping(String filePath) {
		this.filePath = filePath;
		this.attributes = new ArrayList<String>();
		this.sources = new ArrayList<String>();
	}
	
	public Mapping() {
		this.attributes = new ArrayList<String>();
		this.sources = new ArrayList<String>();
	}
	
	public void loadAttributes() throws Exception {
			if(filePath == null || filePath.length() == 0) {
				throw new Exception("File path is null.");
			}
			
			FileReader fReader = new FileReader(filePath);
			BufferedReader bReader = new BufferedReader(fReader);
			String line;
			line = bReader.readLine();
			
			while (line != null) {
				String lineSplit[] = line.split("#");
				String attribute = lineSplit[0];
				int count = 0;
				
				if(lineSplit.length > 1) {
					count = Integer.parseInt(lineSplit[1]);
				}

				if(count > 1) {
					for(int i = 1; i <= count; i++) {
						attributes.add(attribute + "#" + i);
					}
				} else {
					attributes.add(attribute);
				}
				
				line = bReader.readLine();
			}
			bReader.close();
	}
	
	public void loadAttributes(String attributeStr) {
		if(attributeStr != null  && attributeStr.length() > 0) {
			
			String attributeArray[] = attributeStr.split(",");
			
			for(String line : attributeArray) {
				String lineSplit[] = line.split("#");
				String attribute = lineSplit[0];
				int count = 0;
				
				if(lineSplit.length > 1) {
					count = Integer.parseInt(lineSplit[1]);
				}
				
				if(count > 1) {
					for(int i = 1; i <= count; i++) {
						attributes.add(attribute + "#" + i);
					}
				} else {
					attributes.add(attribute);
				}
			}
		}
	}
	
	public void loadSources() throws IOException {
		FileReader fReader = new FileReader(filePath);
		BufferedReader bReader = new BufferedReader(fReader);
		String line;
		line = bReader.readLine();
		while (line != null) {
			sources.add(line);
			line = bReader.readLine();
		}
		bReader.close();
	
	}
	
	public void loadSources(String sourceStr) {
		if(sourceStr != null  && sourceStr.length() > 0) {
			String sourcesArray[] = sourceStr.split(",");
			
			for(String source : sourcesArray) {
				sources.add(source);
			}
		}
	}
}
