# Quick Start 
The utility takes one argument, which is a path to the parameters file. The parameters files links to two other files, the attribute mapping which is required, and the source mapping which is optional.

##Building

The main method of the application is at the following path:
**com/reltio/ps/MergeReport/ThreadMerge.java**

##Dependencies 

1. gson-2.2.4
2. reltio-cst-core-1.4.7

##Parameters File Example

```
#!paintext

#Common Properties
TENANT_ID=swyQZ6vj3SlyfaM
ENVIRONMENT_URL=sndbx.reltio.com
USERNAME=user@reltio.com
PASSWORD=password
CLIENT_CREDENTIALS=*******
AUTH_URL=https://auth.reltio.com/oauth/token

#Tool Specific Properties

MERGE_OUTPUT_FILE_LOCATION=./merge-output
MERGE_TYPE=false
FILE_TYPE=csv
ENTITY_TYPE=Person
FROM_DATE=2017-08-21 17:00:01
TO_DATE=2017-08-30 00:00:01
RECORDS_PER_POST=25
THREAD_COUNT=20
REPORTING_INTERVAL=5000
DEBUG_MODE=FALSE
ATTRIBUTES_MAPPING=./attribute-mapping-1.properties
SOURCE_MAPPING=./source-mapping-1.properties
FILTER=equals(attributes.name,'Bob')
#Default value |
DELIMITER=,

```

##Sample Attribute Mapping 

```
#!plaintext

ConsenttoMergeGlobalFlag
ConsenttoMergeBrandFlag
CustomerSource.Brand
CustomerSource.Country
FirstName
LastName
PreferredName
Phone.Number
Email.Email
Address.AddressLine1
Address.City
Address.StateProvince
Address.Country
Address.Zip5
Identifiers.ID
Identifiers.Type

```

##Sample Source Mapping

```
#!plaintext

Source1
Source2
Source3
```


##Executing

Command to start the utility.
```
#!plaintext

java -jar util-mergereport-${Version}-jar-with-dependencies.jar configuration.properties > $logfilepath$

```
